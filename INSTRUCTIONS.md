# Create a client certificate and private key for the user
openssl genrsa -out username.key 2048
# Common Name and Organisation Name where Organisation Name is condidered as Group.
openssl req -new -key username.key -out name.csr -subj "/CN=username/O=$groupname"
# For multiple Groups
openssl req -new -key name.key -out username.csr -subj "/CN=username/O=$groupname/O=$groupname2"
# Perform the bwlow so that we can put his decoded csr to request field in csr.yaml file. Create a yaml file for csr and paste the base64 version on name.csr in the request field 
cat username.csr | base64 | tr -d '\n'
# Perform below
kubectl apply -f csr.yaml
# perform below
kubectl get csr
# perform below
kubectl certificate approve (name of csr you have given)
# Create a crt file
kubectl get csr username -o jsonpath="{.status.certificate}" | base64 --decode > username.crt
# Alternative method
openssl req -nodes -new -keyout test-user.key -out test-user.csr -subj "/CN=test-user/O=group"
openssl x509 -req -in test-user.csr -CA ~/.minikube/ca.crt -CAkey ~/.minikube/ca.key -CAcreateserial -out test-user.crt -days 365

# Set the credentials for the user in the kubeconfig file
kubectl config set-credentials username --client-certificate=username.crt --client-key=username.key

# Set the context that uses the user
kubectl config set-context test-context --cluster=minikube --user=username--namespace=test

# Use the newly created context
kubectl config use-context test-context
